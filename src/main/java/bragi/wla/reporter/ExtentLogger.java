package bragi.wla.reporter;

import bragi.wla.setpath.VideoPath;
import bragi.wla.utils.PropertyUtils;
import bragi.wla.utils.ScreenshotUtils;
import com.aventstack.extentreports.MediaEntityBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ExtentLogger {
    public static void pass(String message){
        if(PropertyUtils.get("screenshots.for.passed.steps").equalsIgnoreCase("yes")){
            ExtentManager.getExtentTest().pass(message,
                    MediaEntityBuilder.createScreenCaptureFromBase64String(ScreenshotUtils.getBase64Image()).build());
        }else {
            ExtentManager.getExtentTest().pass(message);
        }
    }

    public static void fail(String message){
        ExtentManager.getExtentTest().fail(message, MediaEntityBuilder.createScreenCaptureFromBase64String(ScreenshotUtils.getBase64Image()).build());
    }

    public static void skip(String message){
        ExtentManager.getExtentTest().skip(message, MediaEntityBuilder.createScreenCaptureFromBase64String(ScreenshotUtils.getBase64Image()).build());
    }

    public static void info(String message){
        ExtentManager.getExtentTest().info(message);
    }

    public static void addVideoToReport(){
        ExtentManager.getExtentTest().info("<video width='620' height='340' controls> <source src='" + VideoPath.getVideoPathForReport()
                + "' type='video/mp4'> <videos>");
    }
}
