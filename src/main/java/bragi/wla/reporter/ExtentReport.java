package bragi.wla.reporter;

import bragi.wla.constants.GlobalVars;
import bragi.wla.setpath.ReportPath;
import bragi.wla.utils.PropertyUtils;
import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.JsonFormatter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.reporter.configuration.ViewName;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public final class ExtentReport {

    private ExtentReport(){

    }

    private static ExtentReports extentReports;

    public static void initReports(){
        if(Objects.isNull(extentReports)) {
            // Setting the view order of the tabs in report
            ExtentSparkReporter spark = new ExtentSparkReporter(ReportPath.getReportPath()).viewConfigurer()
                    .viewOrder().as(new ViewName[]{ViewName.DASHBOARD, ViewName.TEST, ViewName.CATEGORY,
                        ViewName.DEVICE, ViewName.AUTHOR})
                    .apply();

            extentReports = new ExtentReports();
            spark.config().setTheme(Theme.DARK);
            spark.config().setDocumentTitle(GlobalVars.getReportTitle());
            spark.config().setReportName(GlobalVars.getReportName());
            spark.config().setEncoding(String.valueOf(StandardCharsets.UTF_8));
            spark.config().setTimeStampFormat(GlobalVars.getDateTimeFormat2());

            //Setting up information to be displayed in the report
            try{
                extentReports.setSystemInfo("Name", PropertyUtils.get("tester.name"));
                extentReports.setSystemInfo("Environment", PropertyUtils.get("environment"));
                if(PropertyUtils.get("platform").equalsIgnoreCase("android")) {
                    extentReports.setSystemInfo("Device Model", PropertyUtils.get("android.device.model"));
                    extentReports.setSystemInfo("Platform Name", PropertyUtils.get("android.platform.name"));
                    extentReports.setSystemInfo("Platform Version", PropertyUtils.get("android.platform.version"));
                }else{
                    extentReports.setSystemInfo("Device Model", PropertyUtils.get("ios.device.name"));
                    extentReports.setSystemInfo("Platform Name", PropertyUtils.get("ios.platform.name"));
                    extentReports.setSystemInfo("Platform Version", PropertyUtils.get("ios.platform.version"));
                }
                extentReports.setSystemInfo("App", PropertyUtils.get("app.name"));
                extentReports.setSystemInfo("App Package", PropertyUtils.get("app.package"));
                extentReports.setSystemInfo("Report Path", ReportPath.getReportPath());
                extentReports.setAnalysisStrategy(AnalysisStrategy.TEST);
                if(PropertyUtils.get("override.reports").equalsIgnoreCase("yes")){
                    JsonFormatter json = new JsonFormatter("old-report.data.json");
                    extentReports.createDomainFromJsonArchive("old-report-data.json");
                    extentReports.attachReporter(json, spark);
                } else {
                    extentReports.attachReporter(spark);
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static void createTests(String testCaseName, String category) {
        ExtentTest extentTest = null;

        try{
            extentTest = extentReports.createTest(testCaseName)
                    .assignAuthor(PropertyUtils.get("tester.name"))
                    .assignCategory(category);
        } catch (Exception e){
            e.printStackTrace();
        }
        ExtentManager.setExtentTest(extentTest);
    }

    public static void flushReport() {
        if(Objects.nonNull(extentReports)){
            extentReports.flush();
            try{
                ExtentManager.unload();
                ExtentReport.addThumbnailToScreenshot(ReportPath.getReportPath());
                Desktop.getDesktop().open(new File(ReportPath.getReportPath()));
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public static void addThumbnailToScreenshot(String reportDir){
        try{
            Document report = Jsoup.parse(new File(reportDir), "UTF-8");
            Elements elements = report.select("a[data-featherlight='image']");
            for (Element element : elements) {
                if (element.attr("href").startsWith("data:image/png;base64")) {
                    String thumbnail = String.format("<img class='r-img' src='%s'>", element.attr("href"));
                    element.select("span").remove();
                    element.append(thumbnail);
                }
            }
            BufferedWriter htmlWriter =
                    new BufferedWriter(new OutputStreamWriter(new FileOutputStream(reportDir), "UTF-8"));
            htmlWriter.write(report.outerHtml());
            htmlWriter.close();
        }catch (FileNotFoundException | UnsupportedEncodingException e){
            e.printStackTrace();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //    Only enable if tests in this class to group into a sub section
//    public static void createTests(String testCaseName, String category) {
//        ExtentTest extentTest = null;
//        ExtentTest extentTestSuite = null;
//
//        try{
//            extentTestSuite = ExtentManager.getExtentTestSuite().createNode(testCaseName);
//        } catch (Exception e){
//            e.printStackTrace();
//        }
//        ExtentManager.setExtentTest(extentTestSuite);
//    }
//
//    public static void createTestSuite(String testCaseName, String category) {
//        ExtentTest extentTest = null;
//
//        try{
//            extentTest = extentReports.createTest(testCaseName)
//                    .assignAuthor(PropertyUtils.get("tester.name"))
//                    .assignCategory(category);
//        } catch (Exception e){
//            e.printStackTrace();
//        }
//        ExtentManager.setExtentTestSuite(extentTest);
//    }
}
