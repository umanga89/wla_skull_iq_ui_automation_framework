package bragi.wla.reporter;

import com.aventstack.extentreports.ExtentTest;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Objects;
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ExtentManager {

    private static ThreadLocal<ExtentTest> extentTest = new ThreadLocal<>();
    public static ExtentTest getExtentTest(){
        return extentTest.get();
    }

    static void setExtentTest(ExtentTest extentTest){
        if(Objects.nonNull(extentTest))
            ExtentManager.extentTest.set(extentTest);
    }

    static void unload(){
        extentTest.remove();
    }

//    Only enable if tests in a single class needs to be grouped to a category

//    private static ThreadLocal<ExtentTest> extentTestSuite = new ThreadLocal<>();
//    public static ExtentTest getExtentTestSuite(){
//        return extentTestSuite.get();
//    }
//
//    static void setExtentTestSuite(ExtentTest extentTest){
//        if(Objects.nonNull(extentTest))
//            ExtentManager.extentTestSuite.set(extentTest);
//    }
//
//    static void unloadTestSuite(){
//        extentTestSuite.remove();
//    }
}
