package bragi.wla.pages.android;

import bragi.wla.pages.BasePage;
import bragi.wla.utils.WaitUtils;
import org.openqa.selenium.By;

public class EarBudsConnectedPageAndroid extends BasePage {


    public EarBudsConnectedPageAndroid() {
    }

    //Earbuds connected page elements
    By title_earbuds_info = By.id("com.bragi.wla.skullcandy.dev:id/cv_earbuds_info");

    By title_earbuds_model = By.id("com.bragi.wla.skullcandy.dev:id/tv_earbuds");

    By text_left_earbud = By.xpath("//android.view.ViewGroup[@resource-id='com.bragi.wla.skullcandy.dev:id/left_battery']//android.widget.TextView[@resource-id='com.bragi.wla.skullcandy.dev:id/tv_side']");

    By text_left_earbud_battery_value = By.xpath("//android.view.ViewGroup[@resource-id='com.bragi.wla.skullcandy.dev:id/left_battery']//android.widget.TextView[@resource-id='com.bragi.wla.skullcandy.dev:id/tv_battery_lvl']");

    By icon_left_earbud_battery_percentage = By.xpath("//android.view.ViewGroup[@resource-id='com.bragi.wla.skullcandy.dev:id/left_battery']//android.widget.TextView[@resource-id='com.bragi.wla.skullcandy.dev:id/tv_percent']");

    By icon_left_earbud_disconnected = By.xpath("//android.view.ViewGroup[@resource-id='com.bragi.wla.skullcandy.dev:id/left_battery']//android.view.View[@resource-id='com.bragi.wla.skullcandy.dev:id/v_line']");

    By text_right_earbud = By.xpath("//android.view.ViewGroup[@resource-id='com.bragi.wla.skullcandy.dev:id/right_battery']//android.widget.TextView[@resource-id='com.bragi.wla.skullcandy.dev:id/tv_side']");

    By text_right_earbud_battery_value = By.xpath("//android.view.ViewGroup[@resource-id='com.bragi.wla.skullcandy.dev:id/right_battery']//android.widget.TextView[@resource-id='com.bragi.wla.skullcandy.dev:id/tv_battery_lvl']");

    By icon_right_earbud_disconnected = By.xpath("//android.view.ViewGroup[@resource-id='com.bragi.wla.skullcandy.dev:id/right_battery']//android.view.View[@resource-id='com.bragi.wla.skullcandy.dev:id/v_line']");

    By icon_right_earbud_battery_percentage = By.xpath("//android.view.ViewGroup[@resource-id='com.bragi.wla.skullcandy.dev:id/right_battery']//android.widget.TextView[@resource-id='com.bragi.wla.skullcandy.dev:id/tv_percent']");

    //Earbud feature elements
    By btn_voice_control = By.xpath("(//*[@resource-id='com.bragi.wla.skullcandy.dev:id/rv_buttons']//android.widget.TextView)[1]");

    //Earbuds connected page actions
    public void waitUntilUserIsViewingEarbudsPage() {
        WaitUtils.waitUntilElementIsPresentOnScreen(title_earbuds_info);
    }

    public boolean isEarbudsInfoIsDisplayedInEarbudsScreen() {
        return isDisplayed(title_earbuds_info);
    }

    public String getEarbudsTitle(){
        return getText(title_earbuds_model);
    }

    public String getLeftEarbudText(){
        return getText(text_left_earbud);
    }

    public String getRightEarbudText(){
        return getText(text_right_earbud);
    }

    public String getLeftEarbudBatteryText(){
        return getText(text_left_earbud_battery_value);
    }

    public String getRightEarbudBatteryText(){
        return getText(text_right_earbud_battery_value);
    }

    public boolean isLeftEarBudBatteryPercentageDisplayed(){
        return isDisplayed(icon_left_earbud_battery_percentage);
    }

    public boolean isRightEarBudBatteryPercentageDisplayed(){
        return isDisplayed(icon_right_earbud_battery_percentage);
    }

    public void clickVoiceControlButton(){
        WaitUtils.waitUntilElementIsClickableOnScreen(btn_voice_control);
        click(btn_voice_control);
    }
}