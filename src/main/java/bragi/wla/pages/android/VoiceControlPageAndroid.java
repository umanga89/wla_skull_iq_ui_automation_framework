package bragi.wla.pages.android;

import bragi.wla.pages.BasePage;
import bragi.wla.utils.WaitUtils;
import org.openqa.selenium.By;

public class VoiceControlPageAndroid extends BasePage {


    public VoiceControlPageAndroid() {
    }

    //Voice Control panel elements
    By panel_voice_control = By.id("com.bragi.wla.skullcandy.dev:id/design_bottom_sheet");
    By title_voice_control = By.xpath("//android.widget.TextView[@text='Voice Control']");

    By title_enable_voice_control = By.xpath("//android.widget.FrameLayout[@resource-id='com.bragi.wla.skullcandy.dev:id/voiceContentNavigationHost']//android.widget.TextView[@resource-id='com.bragi.wla.skullcandy.dev:id/tw_enable_feature']");

    By toggle_enable_voice_control = By.xpath("//android.widget.Switch[@resource-id='com.bragi.wla.skullcandy.dev:id/sc_enable_feature']");

    By btn_voice_language_selector = By.id("com.bragi.wla.skullcandy.dev:id/tv_title_language");

    By checkbox_english_language = By.xpath("//android.widget.TextView[@text='English']/../android.widget.CheckBox");

    By btn_close_voice_control = By.xpath("//android.widget.ImageButton[@content-desc='Close']");

    public void waitUntilUserIsViewingVoiceControlPanel() {
        WaitUtils.waitUntilElementIsPresentOnScreen(panel_voice_control);
    }

    public boolean isUserViewingVoiceControlPanel(){
        WaitUtils.waitUntilElementIsPresentOnScreen(title_voice_control);
        return isDisplayed(title_voice_control);
    }

    public boolean isVoiceControlToggleEnabled(){
        return isToggleChecked(toggle_enable_voice_control);
    }

    public void clickVoiceControlToggle(){
        click(toggle_enable_voice_control);
    }

    public void clickVoiceLanguageSelector(){
        click(btn_voice_language_selector);
    }

    public void clickCloseVoiceControlPanel(){
        click(btn_close_voice_control);
    }

    public boolean isEnglishLanguageSelectedForVoiceControl(){
        WaitUtils.waitUntilElementIsPresentOnScreen(checkbox_english_language);
        return isToggleChecked(checkbox_english_language);
    }


}