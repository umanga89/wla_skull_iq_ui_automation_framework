package bragi.wla.pages.android;

import bragi.wla.driver.AppDriverManager;
import bragi.wla.pages.BasePage;
import bragi.wla.utils.ActionsUtil;
import bragi.wla.utils.AppOperationsUtil;
import bragi.wla.utils.PropertyUtils;
import bragi.wla.utils.WaitUtils;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.time.Duration;
import java.util.List;

public class OnBoardingPagesAndroid extends BasePage {

    public OnBoardingPagesAndroid(){}

    //Welcome screen elements
    By logo_welcome_page = By.id("com.bragi.wla.skullcandy.dev:id/iv_logo");

    By btn_welcome_next = By.id("com.bragi.wla.skullcandy.dev:id/btn_next");

    //T&C page elements
    By slider_agree = By.id("com.bragi.wla.skullcandy.dev:id/thumb");

    By slider_path = By.id("com.bragi.wla.skullcandy.dev:id/tv_slider");

    //Enable bluetooth page elements
    //By title_enable_bluetooth = By.id("com.bragi.wla.skullcandy.dev:id/tv_title");

    By title_enable_bluetooth = By.xpath("//android.widget.TextView[@text='Enable Bluetooth']");

    By btn_ok_sounds_good = By.xpath("//android.widget.Button[@text='Ok, sounds good']");

    By btn_allow_bluetooth = By.id("com.android.permissioncontroller:id/permission_allow_button");

    //Allow file downloads page elements
//    By title_allow_file_downloads = By.id("com.bragi.wla.skullcandy.dev:id/tv_title");
//
//    By text_allow_file_download = By.xpath("(//android.widget.FrameLayout[@resource-id='com.bragi.wla.skullcandy.dev:id/onboardingNavigationFragment']//android.widget.TextView)[2]");
//
//    By btn_continue_allow_downloads = By.xpath("//android.widget.Button[@text='Continue']");

    By title_allow_file_downloads = By.xpath("//android.widget.TextView[@text='Allow file downloads']");

    //By text_allow_file_download = By.xpath("(//android.widget.FrameLayout[@resource-id='com.bragi.wla.skullcandy.dev:id/onboardingNavigationFragment']//android.widget.TextView)[2]");

    By btn_continue_allow_downloads = By.xpath("//android.widget.Button[@text='Continue']");

    By btn_allow_file_download = By.xpath("//android.widget.Button[@text='Allow']");

    //Allow location permission page elements
    By title_allow_location = By.xpath("//android.widget.TextView[@text='Allow location permissions']");

    By text_allow_location = By.xpath("(//android.widget.FrameLayout[@resource-id='com.bragi.wla.skullcandy.dev:id/onboardingNavigationFragment']//android.widget.TextView)[2]");

    By btn_continue_allow_location = By.xpath("//android.widget.TextView[@text='Allow location permissions']/..//android.widget.Button[@text='Continue']");

    By btn_allow_location_while_using_app = By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button");

    //Select your headphones page elements
    By title_select_headphones = By.id("com.bragi.wla.skullcandy.dev:id/tv_title");

    By title_available_devices = By.xpath("//android.widget.TextView[@text='AVAILABLE DEVICES']");
    By btn_connect_skullcandy_grind = By.xpath("//android.widget.TextView[@text='Grind']/../android.widget.TextView[@resource-id='com.bragi.wla.skullcandy.dev:id/connectButtonTextView']");

    By btn_pair_headphones = By.xpath("//android.widget.Button[@text='Pair']");

    //Welcome screen actions
    public void waitUntilUserIsViewingWelcomeScreen(){
        WaitUtils.waitUntilElementIsPresentOnScreen(logo_welcome_page);
    }

    public boolean isSkullCandyLogoIsDisplayedInWelcomeScreen(){
        return isDisplayed(logo_welcome_page);
    }

    public void clickOnContinueButton(){
        click(btn_welcome_next);
    }

    //T&C page actions
    public void agreeToTandCWithSlider(){
        ActionsUtil.moveSliderToEnd(slider_agree, slider_path);
    }

    //Enable bluetooth page actions
    public boolean isEnableBluetoothPageTitleDisplayed(){
        WaitUtils.waitUntilElementIsPresentOnScreen(title_enable_bluetooth);
        return isDisplayed(title_enable_bluetooth);
    }

    public void clickOnOKSoundsGoodButton(){
        click(btn_ok_sounds_good);
    }

    public void clickAllowBluetoothPermissionButton(){
        WaitUtils.waitUntilElementIsPresentOnScreen(btn_allow_bluetooth);
        click(btn_allow_bluetooth);
    }

    //Allow file downloads page actions
    public boolean isEnableFileDownloadsPageTitleDisplayed(){
        WaitUtils.waitUntilElementIsPresentOnScreen(title_allow_file_downloads);
        return isDisplayed(title_allow_file_downloads);
    }

//    public String getAllowDownloadsText(){
//        WaitUtils.waitUntilElementIsVisibleOnScreen(text_allow_file_download);
//        return getText(text_allow_file_download);
//    }

    public void clickOnContinueAllowDownloadsButton(){
        WaitUtils.waitUntilElementIsVisibleOnScreen(title_allow_file_downloads);
        List<WebElement> btn_continue = AppDriverManager.getDriver().findElements(btn_continue_allow_downloads);
        if(btn_continue.isEmpty()){
            AppOperationsUtil.sendSkullIQAppToBackgroundAndBringForwardAndroid(2);
            WaitUtils.waitUntilElementIsVisibleOnScreenIgnoringStaleElementException(btn_continue_allow_downloads);
            ActionsUtil.tapOnElementByCordinates(btn_continue_allow_downloads);
        }else {
            click(btn_continue_allow_downloads);
        }
    }

    public void clickAllowFileDownloadsPermissionButton(){
        WaitUtils.waitUntilElementIsVisibleOnScreen(btn_allow_file_download);
        click(btn_allow_file_download);
    }

    //Allow location permission page actions
    public boolean isEnableLocationPermissionPageTitleDisplayed(){
        WaitUtils.waitUntilElementIsVisibleOnScreen(title_allow_location);
        return isDisplayed(title_allow_location);
    }

    public String getAllowLocationText(){
        WaitUtils.waitUntilElementIsVisibleOnScreen(text_allow_location);
        return getText(text_allow_location);
    }

    public void clickOnContinueAllowLocationButton() {
        WaitUtils.waitUntilElementIsVisibleOnScreen(title_allow_location);
        List<WebElement> btn_continue = AppDriverManager.getDriver().findElements(btn_continue_allow_location);
        if(btn_continue.isEmpty()){
            AppOperationsUtil.sendSkullIQAppToBackgroundAndBringForwardAndroid(2);
            WaitUtils.waitUntilElementIsPresentOnScreen(By.id("com.bragi.wla.skullcandy.dev:id/btn_bottom"));
            ActionsUtil.tapOnElementByCordinates(By.id("com.bragi.wla.skullcandy.dev:id/btn_bottom"));
        }else {
            click(btn_continue_allow_location);
        }
    }

    public void clickAllowLocationPermissionButton(){
        WaitUtils.waitUntilElementIsPresentOnScreen(btn_allow_location_while_using_app);
        click(btn_allow_location_while_using_app);
    }

    //Connect and pair headphones
    public boolean isConnectHeadphonesPageTitleDisplayed(){
        WaitUtils.waitUntilElementIsPresentOnScreen(title_select_headphones);
        return isDisplayed(title_select_headphones);
    }

    public boolean isAvailableDevicesDisplayed(){
        WaitUtils.waitUntilElementIsVisibleOnScreen(title_available_devices);
        return isDisplayed(title_available_devices);
    }

    public void clickOnConnectSkullCandyGrindButton(){
        WaitUtils.waitUntilElementIsVisibleOnScreen(btn_connect_skullcandy_grind);
        click(btn_connect_skullcandy_grind);
    }

    public void clickOnPairButton(){
        WaitUtils.waitUntilElementIsPresentOnScreen(btn_pair_headphones);
        click(btn_pair_headphones);
    }
}
