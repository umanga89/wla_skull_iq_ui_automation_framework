package bragi.wla.pages.android;

import bragi.wla.pages.BasePage;
import bragi.wla.utils.WaitUtils;
import org.openqa.selenium.By;

public class AppTesterPagesAndroid extends BasePage {

    public AppTesterPagesAndroid(){}

    //AppTester elements
    By btn_sign_in_google = By.xpath("//android.widget.Button[@content-desc='Sign in with Google']");

    By btn_select_google_account = By.xpath("//android.widget.TextView[contains(@resource-id,'account_display_name')]");

    By btn_select_skull_iq_dev_version = By.xpath("//android.widget.TextView[contains(@text,'com.bragi.wla.skullcandy.dev')]");

    By chkbox_consent = By.id("dev.firebase.appdistribution:id/consent_checkbox");

    By btn_consent = By.id("dev.firebase.appdistribution:id/consent_button");

    By btn_latest_download = By.xpath("//android.widget.TextView[contains(@text,'Download')]");

    By btn_install = By.xpath("//android.widget.Button[contains(@text,'Install')]");

    By btn_open_installed_app = By.xpath("//android.widget.Button[contains(@text,'Open')]");

    //AppTester actions
    public void clickSignInWithGoogle(){
        WaitUtils.waitUntilElementIsPresentOnScreen(btn_sign_in_google);
        click(btn_sign_in_google);
    }

    public void clickGoogleAccount(){
        WaitUtils.waitUntilElementIsPresentOnScreen(btn_select_google_account);
        click(btn_select_google_account);
    }

    public void clickSkulliQDevVersion(){
        WaitUtils.waitUntilElementIsPresentOnScreen(btn_select_skull_iq_dev_version);
        click(btn_select_skull_iq_dev_version);
    }

    public void clickConsentCheckBox(){
        WaitUtils.waitUntilElementIsPresentOnScreen(chkbox_consent);
        click(chkbox_consent);
    }

    public void clickConsentButton(){
        WaitUtils.waitUntilElementIsPresentOnScreen(btn_consent);
        click(btn_consent);
    }

    public void clickDownloadButtonOfLatestVersion(){
        WaitUtils.waitUntilElementIsPresentOnScreen(btn_latest_download);
        click(btn_latest_download);
    }

    public void clickInstallApp(){
        WaitUtils.waitUntilAppIsInstalledViaAppTester(btn_install);
        click(btn_install);
    }

    public boolean isAppInstalled(){
        WaitUtils.waitUntilAppIsInstalledViaAppTester(btn_open_installed_app);
        return true;
    }
}
