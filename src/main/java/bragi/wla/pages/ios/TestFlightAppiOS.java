package bragi.wla.pages.ios;

import bragi.wla.pages.BasePage;
import bragi.wla.utils.ActionsUtil;
import bragi.wla.utils.WaitUtils;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;

public class TestFlightAppiOS extends BasePage {

    private static AppiumDriver driver;

    public TestFlightAppiOS(){

    }

    By button_install_dev = By.xpath("//XCUIElementTypeStaticText[@name='Skull-iQ Dev']/../..//XCUIElementTypeButton[@name='INSTALL']");

    By button_open_dev = By.xpath("//XCUIElementTypeStaticText[@name='Skull-iQ Dev']/../..//XCUIElementTypeButton[@name='OPEN']");

    public void waitUntilUserIsViewingTestFlightApp(){
        WaitUtils.waitUntilElementIsPresentOnScreen(button_install_dev);
    }

    public void clickInstallDevApp(){
        click(button_install_dev);
    }

    public void waitUntilDevAppInstallationIsComplete(){
        WaitUtils.waitUntilAppIsInstalledViaTestFlight(button_open_dev);
    }

    public boolean isAppInstalled(){
        return isDisplayed(button_open_dev);
    }

}
