package bragi.wla.pages.ios;

import bragi.wla.pages.BasePage;
import bragi.wla.utils.ActionsUtil;
import bragi.wla.utils.WaitUtils;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;

public class OnBoardingPagesiOS extends BasePage {

    private static AppiumDriver driver;

    public OnBoardingPagesiOS(){

    }

    //Welcome screen elements
    By logo_welcome_page = By.xpath("//XCUIElementTypeImage[2]");

    By btn_welcome_next = By.xpath("//XCUIElementTypeStaticText[@name=\"Continue\"]");

    //T&C page elements
    By slider_agree = By.xpath("//XCUIElementTypeStaticText[@name='Slide to agree']/../../XCUIElementTypeImage");

    By slider_path = By.xpath("//XCUIElementTypeStaticText[@name='Slide to agree']/../..");

    //Enable bluetooth page elements
    By title_enable_bluetooth = By.xpath("//XCUIElementTypeStaticText[@name='Enable Bluetooth']");

    By btn_ok_sounds_good = By.xpath("//XCUIElementTypeButton[@name='OK, sounds good']");

    By btn_allow_bluetooth = By.xpath("//XCUIElementTypeButton[@name='OK']");

    //Enable push notifications page elements
    By title_enable_push_notifications = By.xpath("//XCUIElementTypeStaticText[@name='Turn on push notifications']");

    By btn_enable_push_notifications_continue = By.xpath("//XCUIElementTypeButton[@name='Continue']");

    By btn_allow_push_notifications = By.xpath("//XCUIElementTypeButton[@name='Allow']");

    //Let's check your setup page elements
    By title_lets_check_your_setup = By.xpath("//XCUIElementTypeStaticText[contains(@name,'check your setup')]");

    By btn_now_im_connected = By.xpath("//XCUIElementTypeButton[contains(@name,'connected')]");

    //Connect your headphones page elements
    By title_connect_your_headphones = By.xpath("//XCUIElementTypeStaticText[@name='Connect your headphones to the Skull-iQ App']");

    //Welcome screen actions
    public void waitUntilUserIsViewingWelcomeScreen(){
        WaitUtils.waitUntilElementIsPresentOnScreen(logo_welcome_page);
    }

    public boolean isSkullCandyLogoIsDisplayedInWelcomeScreen(){
        return isDisplayed(logo_welcome_page);
    }

    public void clickOnContinueButton(){
        click(btn_welcome_next);
    }

    //T&C page actions
    public void agreeToTandCWithSlider(){
        ActionsUtil.moveSliderToEnd(slider_agree, slider_path);
    }

    //Enable bluetooth page actions
    public boolean isEnableBluetoothPageTitleDisplayed(){
        WaitUtils.waitUntilElementIsPresentOnScreen(title_enable_bluetooth);
        return isDisplayed(title_enable_bluetooth);
    }

    public void clickOnOKSoundsGoodButton(){
        click(btn_ok_sounds_good);
    }

    public void clickAllowBluetoothPermissionButton(){
        WaitUtils.waitUntilElementIsPresentOnScreen(btn_allow_bluetooth);
        click(btn_allow_bluetooth);
    }

    //Enable push notifications page actions
    public boolean isEnablePushNotificationsPageTitleDisplayed(){
        WaitUtils.waitUntilElementIsPresentOnScreen(title_enable_push_notifications);
        return isDisplayed(title_enable_push_notifications);
    }

    public void clickOnPushNotificationsContinueButton(){
        click(btn_enable_push_notifications_continue);
    }

    public void clickAllowPushNotificationsPermissionButton(){
        WaitUtils.waitUntilElementIsPresentOnScreen(btn_allow_push_notifications);
        click(btn_allow_push_notifications);
    }

    //Let's check your setup page actions
    public boolean isLetsCheckYourSetupPageTitleDisplayed(){
        WaitUtils.waitUntilElementIsPresentOnScreen(title_lets_check_your_setup);
        return isEnabled(title_lets_check_your_setup);
    }

    public void clickOnNowImConnectedButton(){
        click(btn_now_im_connected);
    }

    //Connect and pair headphones
    public boolean isConnectHeadphonesPageTitleDisplayed(){
        WaitUtils.waitUntilElementIsPresentOnScreen(title_connect_your_headphones);
        return isDisplayed(title_connect_your_headphones);
    }
}
