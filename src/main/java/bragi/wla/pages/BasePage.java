package bragi.wla.pages;

import bragi.wla.driver.AppDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class BasePage {

    //click an element
    protected void click(By by){
        AppDriverManager.getDriver().findElement(by).click();
        System.out.println("Click has been successfull");
    }

    //check if element is displayed
    protected boolean isDisplayed(By by){
        return AppDriverManager.getDriver().findElement(by).isDisplayed();
    }

    protected boolean isEnabled(By by){
        return AppDriverManager.getDriver().findElement(by).isEnabled();
    }

    //get text
    protected String getText(By by){
        return AppDriverManager.getDriver().findElement(by).getText();
    }

    //Is toggle checked
    protected boolean isToggleChecked(By by){
        WebElement toggle = AppDriverManager.getDriver().findElement(by);
        boolean isToggledChecked = Boolean.parseBoolean(toggle.getAttribute("checked"));
        return isToggledChecked;
    }

}
