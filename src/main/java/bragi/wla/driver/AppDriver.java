package bragi.wla.driver;

import bragi.wla.utils.PropertyUtils;
import io.appium.java_client.Setting;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.options.XCUITestOptions;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Objects;

public class AppDriver {

    public static XCUITestOptions xcuiTestOptions;
    public static UiAutomator2Options uiAutomator2Options;

    public static void android_LaunchAppTester() {
        if (Objects.isNull(AppDriverManager.getDriver())) {
            try {
                uiAutomator2Options = new UiAutomator2Options();
                uiAutomator2Options.setAutomationName(PropertyUtils.get("android.automation.name"));
                uiAutomator2Options.setPlatformName(PropertyUtils.get("android.platform.name"));
                uiAutomator2Options.setPlatformVersion(PropertyUtils.get("android.platform.version"));
                uiAutomator2Options.setDeviceName(PropertyUtils.get("android.device.name"));
                uiAutomator2Options.setAppActivity(PropertyUtils.get("android.apptester.app.activity"));
                uiAutomator2Options.setAppPackage(PropertyUtils.get("android.apptester.app.package"));

                AppDriverManager.setDriver(new AndroidDriver(new URL("http://0.0.0.0:4723/"), uiAutomator2Options));

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }
    public static void android_LaunchApp() {
        if (Objects.isNull(AppDriverManager.getDriver())) {
            try {
                uiAutomator2Options = new UiAutomator2Options();
 //               uiAutomator2Options.setApp(PropertyUtils.get("android.app.location"));
                uiAutomator2Options.setAutomationName(PropertyUtils.get("android.automation.name"));
                uiAutomator2Options.setPlatformName(PropertyUtils.get("android.platform.name"));
                uiAutomator2Options.setPlatformVersion(PropertyUtils.get("android.platform.version"));
                uiAutomator2Options.setDeviceName(PropertyUtils.get("android.device.name"));
                //to be used if working with already installed app
                uiAutomator2Options.setAppActivity(PropertyUtils.get("android.app.activity"));
                uiAutomator2Options.setAppPackage(PropertyUtils.get("android.app.package"));
                //Use of elements are not loaded on initial UI change
 //               uiAutomator2Options.disableWindowAnimation();

                AppDriverManager.setDriver(new AndroidDriver(new URL("http://0.0.0.0:4723/"), uiAutomator2Options));

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void android_LaunchApp_no_rest_true() {
        if (Objects.isNull(AppDriverManager.getDriver())) {
            try {
                uiAutomator2Options = new UiAutomator2Options();
//                uiAutomator2Options.setApp(PropertyUtils.get("android.app.location"));
                uiAutomator2Options.setAutomationName(PropertyUtils.get("android.automation.name"));
                uiAutomator2Options.setPlatformName(PropertyUtils.get("android.platform.name"));
                uiAutomator2Options.setPlatformVersion(PropertyUtils.get("android.platform.version"));
                uiAutomator2Options.setDeviceName(PropertyUtils.get("android.device.name"));
                uiAutomator2Options.setNoReset(true);
                uiAutomator2Options.setFullReset(false);
                uiAutomator2Options.setNewCommandTimeout(Duration.ofSeconds(1200));
                //to be used if working with already installed app
                uiAutomator2Options.setAppActivity(PropertyUtils.get("android.app.activity"));
                uiAutomator2Options.setAppPackage(PropertyUtils.get("android.app.package"));

                AppDriverManager.setDriver(new AndroidDriver(new URL("http://0.0.0.0:4723/"), uiAutomator2Options));
                //To reduce tests running slow
                AppDriverManager.getDriver().setSetting(Setting.WAIT_FOR_IDLE_TIMEOUT, 1);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void iOS_LaunchTestFlight() {
        if (Objects.isNull(AppDriverManager.getDriver())) {
            try {
                xcuiTestOptions = new XCUITestOptions();
                xcuiTestOptions.setAutomationName(PropertyUtils.get("ios.automation.name"));
                xcuiTestOptions.setPlatformName(PropertyUtils.get("ios.platform.name"));
                xcuiTestOptions.setPlatformVersion(PropertyUtils.get("ios.platform.version"));
                xcuiTestOptions.setDeviceName(PropertyUtils.get("ios.device.name"));
                xcuiTestOptions.setUdid(PropertyUtils.get("ios.device.udid"));
                xcuiTestOptions.setBundleId(PropertyUtils.get("ios.testflight.bundle.id"));

                AppDriverManager.setDriver(new IOSDriver(new URL("http://0.0.0.0:4723/"), xcuiTestOptions));
            }catch (MalformedURLException e){
                e.printStackTrace();
            }
        }
    }

    public static void iOS_LaunchApp() {
        if (Objects.isNull(AppDriverManager.getDriver())) {
            try {
                xcuiTestOptions = new XCUITestOptions();
 //               xcuiTestOptions.setApp(PropertyUtils.get("ios.app.location"));
                xcuiTestOptions.setAutomationName(PropertyUtils.get("ios.automation.name"));
                xcuiTestOptions.setPlatformName(PropertyUtils.get("ios.platform.name"));
                xcuiTestOptions.setPlatformVersion(PropertyUtils.get("ios.platform.version"));
                xcuiTestOptions.setDeviceName(PropertyUtils.get("ios.device.name"));
                xcuiTestOptions.setUdid(PropertyUtils.get("ios.device.udid"));
                xcuiTestOptions.setBundleId(PropertyUtils.get("ios.bundle.id"));

                AppDriverManager.setDriver(new IOSDriver(new URL("http://0.0.0.0:4723/"), xcuiTestOptions));
            }catch (MalformedURLException e){
                e.printStackTrace();
            }
        }
    }

    public static void unlockDevice_android(){
        if(Objects.nonNull(AppDriverManager.getDriver())) {
            ((AndroidDriver)AppDriverManager.getDriver()).unlockDevice();
        }
    }

    public static void closeApp(){
        if(Objects.nonNull(AppDriverManager.getDriver())) {
            AppDriverManager.getDriver().quit();
            AppDriverManager.unload();
        }
    }

    public static void terminateApp_android(){
        if(Objects.nonNull(AppDriverManager.getDriver())) {
            ((AndroidDriver)AppDriverManager.getDriver()).terminateApp(PropertyUtils.get("app.package"));
        }
    }
}
