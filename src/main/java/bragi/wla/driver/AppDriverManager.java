package bragi.wla.driver;

import io.appium.java_client.AppiumDriver;

public class AppDriverManager {

    public static ThreadLocal<AppiumDriver> driver = new ThreadLocal<>();

    public static AppiumDriver getDriver() {
        return driver.get();
    }

    static void setDriver(AppiumDriver driver) {
        AppDriverManager.driver.set(driver);
    }

    public static void unload(){
        driver.remove();
    }
}
