package bragi.wla.setpath;

import bragi.wla.constants.GlobalVars;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class VideoPath {

    private static ThreadLocal<String> videoFullPath = new ThreadLocal<>();
    private static ThreadLocal<String> videoReportPath = new ThreadLocal<>();

    public static void setVideoPath(String testName){
        videoFullPath.set(GlobalVars.getVideoDir() + "/" + testName +".mp4");
    }

    public static String getVideoPath(){
        return videoFullPath.get();
    }

    public static void flushVideoPath(){
        videoFullPath.remove();
    }

    public static void setVideoPathForReport(String testName){
        videoReportPath.set(GlobalVars.getVideoDirForReport() + "/" + testName +".mp4");
    }

    public static String getVideoPathForReport(){
        return videoReportPath.get();
    }

    public static void flushVideoPathForReport(){
        videoReportPath.remove();
    }
}
