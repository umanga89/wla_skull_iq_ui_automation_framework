package bragi.wla.setpath;

import bragi.wla.constants.GlobalVars;
import bragi.wla.utils.PropertyUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ReportPath {

    private static String reportFilePath = "";

    public static String getReportPath(){
        if(reportFilePath.isEmpty()) {
            reportFilePath = setReportPath()+".html";
        }
        return reportFilePath;
    }

    private static String setReportPath(){
        if(PropertyUtils.get("override.reports").equalsIgnoreCase("no")){
            return GlobalVars.getReportDir() + "/ExecutionReport_"
                    + new SimpleDateFormat(GlobalVars.getDateTimeFormat1()).format(new Date());
        } else{
            return GlobalVars.getReportDir().concat("/index.html");
        }
    }
}
