package bragi.wla.setpath;

import bragi.wla.constants.GlobalVars;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ScreenshotPath {

    private static ThreadLocal<String> dir = new ThreadLocal<>();

    public static void unloadDir(){
        dir.remove();
    }

    public static String getCurrentTestExecutionScreenShotsDir(){
        return dir.get();
    }

    //need to add another param here such as platform
    public static void setCurrentTestExecutionScreenshotsDir(){
        dir.set(GlobalVars.getScreenshotsDir() + new SimpleDateFormat(GlobalVars.getDateTimeFormat1()).format(new Date()));
    }
}
