import os.path
import file_operations
import sys

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/drive']


def upload_report_to_google_drive_location(platform):
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('src/main/java/bragi/wla/uploadreport/token.json'):
        creds = Credentials.from_authorized_user_file('src/main/java/bragi/wla/uploadreport/token.json')
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'src/main/java/bragi/wla/uploadreport/credentials.json')
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('src/main/java/bragi/wla/uploadreport/token.json', 'w') as token:
            token.write(creds.to_json())

    try:
        # create drive api client
        service = build('drive', 'v3', credentials=creds)
        folder_id = file_operations.create_a_new_folder_with_todays_date(platform, service)

        file_operations.upload_report_zip_to_google_drive(folder_id, service)

    except HttpError as error:
        print(F'An error occurred: {error}')
        file = None


upload_report_to_google_drive_location(sys.argv[1])