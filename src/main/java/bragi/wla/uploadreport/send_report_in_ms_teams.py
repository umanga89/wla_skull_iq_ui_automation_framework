import sys
import pymsteams


def send_report_in_ms_teams(current_report_url, all_reports_url):
    myTeamsMessage = pymsteams.connectorcard(
        'https://bragi.webhook.office.com/webhookb2/9153c19d-d77b-4f2d-b908-9559b8aee970@5fa57aa5-c25f-4156-b623'
        '-1b6ae7c156ff/IncomingWebhook/f848a1eff39643b29d6f43cfb549f69d/8d882355-9347-4ffd-9061-5764ff82085b')
    myTeamsMessage.text(
        'WLA Automated TestSuite has been executed successfully.   \nThe latest test report can be found in '
        + current_report_url +
        '   \nHere is the link to all reports ' + all_reports_url)
    myTeamsMessage.send()
    print('Message sent successfully!')


current_report_url = sys.argv[1]
all_reports_url = sys.argv[2]
send_report_in_ms_teams(current_report_url, all_reports_url)
