from googleapiclient.errors import HttpError
from datetime import datetime

from googleapiclient.http import MediaFileUpload


def create_a_new_folder_with_todays_date(platform, service):
    if platform == 'android':
        folder_id = '15SQruphENBcSDL_2c4KVAZ8U6JNu-Rsa'
    else:
        folder_id = '1Azv3zcgVIr5zCfU2qvxODeFCIhcs6rwK'
    try:
        # Call the Drive v3 API
        today = datetime.now()
        folder_name = today.strftime("%d/%m/%Y %H:%M:%S")
        file_metadata = {
            'name': folder_name,
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': [folder_id]
        }
        file = service.files().create(body=file_metadata).execute()
        return file.get('id')

    except HttpError as error:
        print(F'An error occurred: {error}')
        file = None


def upload_report_zip_to_google_drive(folder_id, service):

    try:

        file_metadata = {
            'name': 'test-reports.zip',
            'parents': [folder_id]
        }
        media = MediaFileUpload('test-report-zipped/test_report.zip',
                                mimetype='application/zip')
        # pylint: disable=maybe-no-member
        file = service.files().create(body=file_metadata, media_body=media).execute()
        print(F'File ID: {file.get("id")}')
        print(F'Folder ID: {folder_id}')

    except HttpError as error:
        print(F'An error occurred: {error}')
        file = None

    return file.get('id')
