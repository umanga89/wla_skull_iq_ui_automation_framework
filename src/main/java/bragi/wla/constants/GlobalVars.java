package bragi.wla.constants;

import lombok.Getter;
import lombok.experimental.UtilityClass;

@UtilityClass
public class GlobalVars {

    //Directory Related
    @Getter final String userDir = System.getProperty("user.dir");
    @Getter final String resourcesPath = userDir.concat("/src/test/resources");
    @Getter final String screenshotsDir = userDir.concat("/test-screenshots/");
    @Getter final String reportDir = userDir.concat("/test-reports");
    @Getter final String reportZippedDir = userDir.concat("/test-report-zipped");
    @Getter final String videoDir = userDir.concat("/test-reports/test-report-videos");
    @Getter final String videoDirForReport = "test-report-videos";
    @Getter final String logDir = userDir.concat("/test-log");
    @Getter final String zippedReportLocation = userDir.concat("/test-report-zipped/test_report.zip");

    //Properties Related
    @Getter final String configProp = resourcesPath.concat("/config/config.properties");
    @Getter final String pomProp = resourcesPath.concat("/config/pom.properties");

    //Appium Driver Related
    @Getter final int EXPLICIT_WAIT_TIMEOUT = 30;
    @Getter final int TESTFLIGHT_WAIT_TIMEOUT = 60;
    @Getter final int APPTESTER_WAIT_TIMEOUT = 90;

    //Test Report Related
    @Getter final String reportName = "Automated Test Report";
    @Getter final String reportTitle = "Automated Test Report";

    //Google Drive Related
    @Getter final String googleDriveBaseUrl = "https://drive.google.com/drive/folders/";
    @Getter final String androidReports = googleDriveBaseUrl.concat("15SQruphENBcSDL_2c4KVAZ8U6JNu-Rsa");
    @Getter final String iOSReports = googleDriveBaseUrl.concat("1Azv3zcgVIr5zCfU2qvxODeFCIhcs6rwK");

    //Misc
    @Getter final String dateTimeFormat1 = "dd_MM_yyyy_hh_mm_ss_SSS";
    @Getter final String dateTimeFormat2 = "dd/MM/yyyy HH:mm:ss";
}
