package bragi.wla.listeners;

import bragi.wla.Appium.AppiumServer;
import bragi.wla.Appium.ServiceManager;
import bragi.wla.constants.GlobalVars;
import bragi.wla.reporter.ExtentLogger;
import bragi.wla.reporter.ExtentManager;
import bragi.wla.reporter.ExtentReport;
import bragi.wla.setpath.VideoPath;
import bragi.wla.utils.*;
import lombok.extern.log4j.Log4j2;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Base64;

@Log4j2
public class Listener implements ITestListener, ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        try {
            if (PropertyUtils.get("send.report.in.email").equalsIgnoreCase("yes")) {
                Base64.getDecoder().decode(PropertyUtils.get("email.password"));
            }
        } catch (Exception e) {
            UserInputCheck.designerOutputForPasswordError();
            System.exit(1);
        }

        //Adding a property to check if App needs to be installed - Driver will be launched based on this value
        PropertyUtils.set("app.installed", "no");

        //Adding a property to check if onBoarding needs to be run - Driver will be launched based on this value
        PropertyUtils.set("initial.run", "yes");

        //Enable if need to archive old reports
        //FileSystemHandlerUtil.deleteOldReports();
        //FileSystemHandlerUtil.deleteDir(GlobalVars.getScreenshotsDir());
        try {
            FileSystemHandlerUtil.deleteDir(GlobalVars.getReportDir());
            FileSystemHandlerUtil.deleteDir(GlobalVars.getVideoDir());
            FileSystemHandlerUtil.deleteDir(GlobalVars.getReportZippedDir());
            FileSystemHandlerUtil.deleteOldLogFiles();
            FileSystemHandlerUtil.createRequiredDirs();
            ExtentReport.initReports();
            AppiumServer.startAppiumServer();
            log.info("Server URL "+ ServiceManager.getService().getUrl());
            log.info("All directories have been created!");
            log.info("Test suite has been started!");
        }catch (Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    @Override
    public void onFinish(ISuite suite) {
        ExtentReport.flushReport();
        FileSystemHandlerUtil.createZipFileForReport();
        UploadReportToGoogleDriveUtil.uploadReport();
        try {
            AppiumServer.stopServer();
            //UserInputCheck.sendTestReportOnEmail();
        }catch (Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
        log.info("Test Suite is complete!!!!!!!!!!!!!!");
    }

    @Override
    public void onTestStart(ITestResult result) {
        VideoPath.setVideoPath(result.getMethod().getDescription());
        VideoPath.setVideoPathForReport(result.getMethod().getDescription());
        ExtentReport.createTests(result.getMethod().getDescription(), result.getTestClass().getRealClass().getSimpleName());
        if(PropertyUtils.get("platform").equalsIgnoreCase("android")) {
            ExtentManager.getExtentTest().assignDevice(PropertyUtils.get("android.device.model"));
            if(PropertyUtils.get("run.mode").equalsIgnoreCase("local")) {
                RecordingUtil.startRecording_android();
            }
        }else{
            ExtentManager.getExtentTest().assignDevice(PropertyUtils.get("ios.device.name"));
            if(PropertyUtils.get("run.mode").equalsIgnoreCase("local")) {
                RecordingUtil.startRecording_iOS();
            }
        }

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        ExtentLogger.info("Testcase: " + (result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Test.class).testName())
                + " has been completed successfully!" + "<br/> Status: Pass");
        log.info("Testcase: " + (result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Test.class).testName())
                + " has been completed successfully!" + "<br/> Status: Pass");

        if(PropertyUtils.get("run.mode").equalsIgnoreCase("local")) {
            RecordingUtil.stopRecording(result.getTestName());
            ExtentLogger.addVideoToReport();
        }
    }

    @Override
    public void onTestFailure(ITestResult result) {
        ExtentLogger.fail(result.getThrowable().toString() + "<br/><br/>"
                + Arrays.toString(result.getThrowable().getStackTrace()));

        ExtentLogger.info("Testcase: "
                + (result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Test.class).testName())
                + "<br/>" + (result.getClass()).toString().replace("class", "Class: ") + "<br/>" + "Method: "
                + result.getMethod().getMethodName() + "<br/> Status: Fail<br/>");

        log.error(result.getThrowable().toString() + "<br/><br/>"
                + Arrays.toString(result.getThrowable().getStackTrace()));

        log.info("Testcase: "
                + (result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Test.class).testName())
                + "<br/>" + (result.getClass()).toString().replace("class", "Class: ") + "<br/>" + "Method: "
                + result.getMethod().getMethodName() + "<br/> Status: Fail<br/>");

        if(PropertyUtils.get("run.mode").equalsIgnoreCase("local")) {
            RecordingUtil.stopRecording(result.getTestName());
            ExtentLogger.addVideoToReport();
        }
    }

    public void onTestSkipped(ITestResult result) {
        ExtentLogger.skip("Testcase: "
                + (result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Test.class).testName())
                + "<br/>" + (result.getClass()).toString().replace("class", "Class: ") + "<br/>" + "Method: "
                + result.getMethod().getMethodName() + "<br/> Status: Skip");

        ExtentLogger.info("Testcase: "
                + (result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Test.class).testName())
                + "<br/>" + (result.getClass()).toString().replace("class", "Class: ") + "<br/>" + "Method: "
                + result.getMethod().getMethodName() + "<br/> Status: Skip");

        if(PropertyUtils.get("run.mode").equalsIgnoreCase("local")) {
            RecordingUtil.stopRecording(result.getTestName());
            ExtentLogger.addVideoToReport();
        }
    }
}
