package bragi.wla.utils;

import bragi.wla.setpath.ReportPath;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Base64;
import java.util.Properties;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SendReportUtil {

    static String encodedPassword;
    static String receiversEmailId;
    static String emailId;
    static String report;
    static String reportName;

    static {
        encodedPassword = PropertyUtils.get("email.password");
        receiversEmailId = PropertyUtils.get("receiver.email");
        emailId = PropertyUtils.get("sender.email");
        report = ReportPath.getReportPath();
        reportName = report.split("/")[report.split("/").length - 1];
    }

    public static void sendMail() {
        try {
            if (PropertyUtils.get("send.report.in.email")
                    .equalsIgnoreCase("yes")) {

                Properties props = System.getProperties();
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                props.put("mail.smtp.ssl.checkserveridentity", true);

                if (PropertyUtils.get("email.client").equalsIgnoreCase("Gmail")) {
                    props.put("mail.smtp.host", "smtp.gmail.com");
                    props.put("mail.smtp.port", "465");
                } else if (PropertyUtils.get("email.client").equalsIgnoreCase("Outlook")) {
                    props.put("mail.smtp.host", "smtp.live.com");
                    props.put("mail.smtp.port", "587");
                }

                Session session = Session.getDefaultInstance(props, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        try {
                            String decodedPassword = new String(Base64.getDecoder().decode(encodedPassword.getBytes()));
                            return new PasswordAuthentication(emailId, "yhxwvhwhrwwwbema");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });
                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(emailId));
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiversEmailId));
                message.setSubject("Test Execution Report - " + reportName);

                BodyPart body = new MimeBodyPart();
                body.setText("Hi,\nPlease find attached Automated test report for "
                        + PropertyUtils.get("environment") + ".\nThanks & regards\n");
                MimeBodyPart mimeBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(report);
                mimeBodyPart.setDataHandler(new DataHandler(source));
                mimeBodyPart.setFileName(reportName);
                Multipart multipart = new MimeMultipart();
                multipart.addBodyPart(body);
                multipart.addBodyPart(mimeBodyPart);
                message.setContent(multipart);
                Transport.send(message);
                System.out.println("\n\n");
                System.out.println("----------------------------");
                System.out.println("|                          |");
                System.out.println("| Report sent successfully |");
                System.out.println("|                          |");
                System.out.println("----------------------------");
                System.out.println("\n\n");
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
