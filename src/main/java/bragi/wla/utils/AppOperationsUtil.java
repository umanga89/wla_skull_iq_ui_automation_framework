package bragi.wla.utils;

import bragi.wla.driver.AppDriverManager;
import io.appium.java_client.android.AndroidDriver;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.time.Duration;

@Log4j2
public class AppOperationsUtil {

    public static void uninstallSkullCandyAppiOS(){
        Runtime runtime = Runtime.getRuntime();
        try{
            String[] command = {"ideviceinstaller", "-U", "com.iphone.Skull-iQ.dev"};
            runtime.exec(command);
            log.info("App has been installed successfully from device");
        } catch (IOException e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    public static void uninstallSkullCandyAppAndroid(){
        Runtime runtime = Runtime.getRuntime();
        try{
            String[] command = {"adb", "uninstall", "com.bragi.wla.skullcandy.dev"};
            runtime.exec(command);
            log.info("App has been uninstalled successfully from device");
        } catch (IOException e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    public static void sendSkullIQAppToBackgroundAndBringForwardAndroid(int seconds){
        ((AndroidDriver) AppDriverManager.getDriver()).runAppInBackground(Duration.ofSeconds(seconds));
        ((AndroidDriver)AppDriverManager.getDriver()).activateApp(PropertyUtils.get("android.app.package"));
    }
}
