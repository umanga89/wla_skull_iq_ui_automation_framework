package bragi.wla.utils;

import bragi.wla.constants.GlobalVars;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FileSystemHandlerUtil {

    public static void deleteOldReports(){
        File targetDirForReport = new File(GlobalVars.getReportDir());
        File targetDirForVideos = new File(GlobalVars.getVideoDir());

        if(PropertyUtils.get("delete.old.reports").equalsIgnoreCase("yes")){
            if(targetDirForReport.exists()) {
                deleteOldFiles(targetDirForReport.listFiles());
            }

            if (targetDirForVideos.exists()){
                deleteOldFiles(targetDirForVideos.listFiles());
            }
        }
    }

    public static void deleteOldLogFiles() throws ParseException {
        File targetDirForLogs = new File(GlobalVars.getLogDir());
        Long time = new Date().getTime();
        Date dateOnMidnight = new Date(time - time % (24 * 60 * 60 * 1000));
        Long purgeTime = dateOnMidnight.getTime();
        for(File file : targetDirForLogs.listFiles()){
            System.out.println(file.lastModified());
            if(file.lastModified() < purgeTime) {
                file.delete();
            }
        }
    }

    private static void deleteOldFiles(File[] filesToDelete){
        int daysBack = Integer.parseInt(PropertyUtils.get("number.of.days"));
        long purgeTime = System.currentTimeMillis() - ((long) daysBack * 24 * 60 * 60 * 1000);
        for(File report : filesToDelete){
            if(report.lastModified() < purgeTime) {
                report.delete();
            }
        }
    }

    public static void deleteDir(String path){
        try{
            if(new File(path).exists())
                FileUtils.deleteDirectory(new File(path));
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void createDir(String path){
        try{
            if(!new File(path).exists())
                FileUtils.forceMkdir(new File(path));
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void copyFile(String from, String to){
        try{
            if(new File(from).exists()){
                FileUtils.copyFile(new File(from), new File(to));
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void createRequiredDirs(){
        String[] dirsToCreate = { GlobalVars.getVideoDir(), GlobalVars.getReportDir(), GlobalVars.getReportZippedDir()};
        for (int i = 0; i < dirsToCreate.length; i++) {
            File dir = new File(dirsToCreate[i]);
            if (!dir.exists()) {
                dir.mkdirs();
            }
        }
        System.out.println("Required directories has been created successfully!!!!!!");
    }

    public static void createZipFileForReport(){
        try {
            ZipUtil.pack(new File(GlobalVars.getReportDir()), new File(GlobalVars.getZippedReportLocation()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
