package bragi.wla.utils;

import bragi.wla.constants.GlobalVars;
import bragi.wla.driver.AppDriverManager;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class WaitUtils {

    public static WebElement waitUntilElementIsPresentOnScreen(By by){
        return new WebDriverWait(AppDriverManager.getDriver(), Duration.ofSeconds(GlobalVars.getEXPLICIT_WAIT_TIMEOUT()))
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public static WebElement waitUntilElementIsClickableOnScreen(By by){
        return new WebDriverWait(AppDriverManager.getDriver(), Duration.ofSeconds(GlobalVars.getEXPLICIT_WAIT_TIMEOUT()))
                .until(ExpectedConditions.elementToBeClickable(by));
    }

    public static WebElement waitUntilElementIsVisibleOnScreen(By by){
        return new WebDriverWait(AppDriverManager.getDriver(), Duration.ofSeconds(GlobalVars.getEXPLICIT_WAIT_TIMEOUT()))
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static WebElement waitUntilAppIsInstalledViaTestFlight(By by){
        return new WebDriverWait(AppDriverManager.getDriver(), Duration.ofSeconds(GlobalVars.getTESTFLIGHT_WAIT_TIMEOUT()))
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static WebElement waitUntilAppIsInstalledViaAppTester(By by){
        return new WebDriverWait(AppDriverManager.getDriver(), Duration.ofSeconds(GlobalVars.getAPPTESTER_WAIT_TIMEOUT()))
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static WebElement waitUntilElementIsVisibleOnScreenIgnoringStaleElementException(By by){
        return new WebDriverWait(AppDriverManager.getDriver(), Duration.ofSeconds(GlobalVars.getEXPLICIT_WAIT_TIMEOUT())).until(ExpectedConditions.refreshed(
                ExpectedConditions.visibilityOfElementLocated(by)));
    }

    public static WebElement waitUntilElementIsVisibleOnScreenByElement(WebElement element){
        return new WebDriverWait(AppDriverManager.getDriver(), Duration.ofSeconds(GlobalVars.getEXPLICIT_WAIT_TIMEOUT()))
                .until(ExpectedConditions.visibilityOf(element));
    }
}
