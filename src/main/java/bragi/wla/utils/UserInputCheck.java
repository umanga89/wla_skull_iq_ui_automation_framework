package bragi.wla.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UserInputCheck {

	public static void sendTestReportOnEmail() {
		try {
			SendReportUtil.sendMail();
		} catch (Exception e) {
			designerOutputForErrorInSendingReport();
		}
	}

	public static void designerOutputForPasswordError() {
		System.out.println("\n\n");
		System.out.println("-----------------------------------------");
		System.out.println("|                                       |");
		System.out.println("| Please enter password only in Base64  |");
		System.out.println("| and re-run the test                   |");
		System.out.println("|                                       |");
		System.out.println("-----------------------------------------");
		System.out.println("\n\n");
	}

	public static void designerOutputForErrorInSendingReport() {
		System.out.println("\n\n");
		System.out.println("-------------------------------------------");
		System.out.println("|                                         |");
		System.out.println("| An error occured while sending report.  |");
		System.out.println("| Please check.                        .  |");
		System.out.println("|                                         |");
		System.out.println("-------------------------------------------");
		System.out.println("\n\n");
	}
}