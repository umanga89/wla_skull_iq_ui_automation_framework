package bragi.wla.utils;

import bragi.wla.driver.AppDriverManager;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;

import java.time.Duration;
import java.util.Arrays;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ActionsUtil {

    public static void moveSliderToEnd(By sliderBy, By sliderPathBy){
        WebElement sliderElement = AppDriverManager.getDriver().findElement(sliderBy);
        WebElement sliderElementPath = AppDriverManager.getDriver().findElement(sliderPathBy);

        int centerY = sliderElement.getRect().y + (sliderElement.getSize().height/2);
        double startX = sliderElementPath.getRect().x + (sliderElementPath.getSize().width * 0.1);
        double endX = sliderElementPath.getRect().x + (sliderElementPath.getSize().width * 0.99);

        PointerInput finger = new PointerInput(PointerInput.Kind.TOUCH, "finger");


        Sequence swipe = new Sequence(finger, 1);
        swipe.addAction(finger.createPointerMove(Duration.ofSeconds(0), PointerInput.Origin.viewport(), (int)startX, centerY));
        swipe.addAction(finger.createPointerDown(0));
        swipe.addAction(finger.createPointerMove(Duration.ofMillis(700), PointerInput.Origin.viewport(), (int)endX, centerY));
        swipe.addAction(finger.createPointerUp(0));

        AppDriverManager.getDriver().perform(Arrays.asList(swipe));
    }

    public static void tapOnElementByCordinates(By element){
        WebElement elementToTap = AppDriverManager.getDriver().findElement(element);
        int startX = elementToTap.getRect().x + (elementToTap.getSize().width/2);
        int centerY = elementToTap.getRect().y + (elementToTap.getSize().height/2);

        PointerInput finger = new PointerInput(PointerInput.Kind.TOUCH, "finger");


        Sequence swipe = new Sequence(finger, 1);
        swipe.addAction(finger.createPointerMove(Duration.ofSeconds(0), PointerInput.Origin.viewport(), (int)startX, centerY));
        swipe.addAction(finger.createPointerDown(0));
        swipe.addAction(finger.createPointerUp(0));

        AppDriverManager.getDriver().perform(Arrays.asList(swipe));
    }
}
