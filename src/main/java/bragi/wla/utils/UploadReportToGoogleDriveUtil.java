package bragi.wla.utils;

import bragi.wla.constants.GlobalVars;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.zeroturnaround.zip.ZipUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UploadReportToGoogleDriveUtil {

    public static void uploadReport(){
        try {
            log.info("Starting report upload");
            String platform = PropertyUtils.get("platform").toLowerCase();
            String[] cmd = {"/Library/Frameworks/Python.framework/Versions/3.11/bin/python3", "src/main/java/bragi/wla/uploadreport/main.py", platform};
            String output = executeCommandLineCommands(cmd);
            boolean isReportUploaded = false;
            if(output.contains("File ID:")){
                isReportUploaded = true;
            }
            Assert.assertTrue(isReportUploaded, "Uploading report has failed!");
            log.info("Report has been uploaded successfully!");
            createMsTeamsMessage(output);
        }catch (Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    public static void createMsTeamsMessage(String folderIdString){


        try {
            log.info("Starting ms teams broadcast");
            String[] twoLines = folderIdString.split("\n");
            String[] secondLine = twoLines[1].split(":");
            String reportUrl = GlobalVars.getGoogleDriveBaseUrl()+secondLine[1].trim();
            String allReportsUrl = GlobalVars.getAndroidReports();
            if(!PropertyUtils.get("platform").equalsIgnoreCase("android")){
                allReportsUrl = GlobalVars.getIOSReports();
            }
            String[] cmd = {"/Library/Frameworks/Python.framework/Versions/3.11/bin/python3", "src/main/java/bragi/wla/uploadreport/send_report_in_ms_teams.py", reportUrl, allReportsUrl};
            String output = executeCommandLineCommands(cmd);
            boolean isMessageSent = false;
            if(output.contains("successfully")){
                isMessageSent = true;
            }
            Assert.assertTrue(isMessageSent, "ms teams broadcast failed!");
            log.info("MS Teams message broadcasted successfully!");
        }catch (Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    public static String executeCommandLineCommands(String[] cmd){
        String output = "";
        try{
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();

            String line = "";
            StringBuilder sb = new StringBuilder();

            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = br.readLine())!= null) {sb = sb.append(line).append("\n"); }

            output = sb.toString();
            log.info(output);
        }catch (Exception e){
            e.printStackTrace();
        }
        return output;
    }
}
