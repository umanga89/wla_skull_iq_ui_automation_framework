package bragi.wla.utils;

import bragi.wla.driver.AppDriverManager;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ScreenshotUtils {

    public static String getBase64Image(){
        //getImageAsFile(ScreenshotPath.getCurrentTestExecutionScreenShotsDir());
        return ((TakesScreenshot) AppDriverManager.getDriver()).getScreenshotAs(OutputType.BASE64);
    }

    static void getImageAsFile(String dir){
        String screenshotName = dir + "/screenshot_";
        File ts = ((TakesScreenshot) AppDriverManager.getDriver()).getScreenshotAs(OutputType.FILE);

        try{
            BufferedImage inputImage = ImageIO.read(ts.getAbsoluteFile());
            BufferedImage outputImage = new BufferedImage(1920, 888, inputImage.getType());
            Graphics2D g2d = outputImage.createGraphics();
            g2d.drawImage(inputImage, 0,0,1920,888, null);
            g2d.dispose();
            ImageIO.write(outputImage, "png", new File(screenshotName));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
