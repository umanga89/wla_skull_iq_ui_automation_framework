package bragi.wla.utils;

import bragi.wla.constants.GlobalVars;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PropertyUtils {

    private static Properties property = new Properties();
    private static final Map<String, String> CONFIGMAP = new HashMap<>();

    static{
        try{
            FileInputStream fis = new FileInputStream(GlobalVars.getConfigProp());
            property.load(fis);
            FileInputStream fis2 = new FileInputStream(GlobalVars.getPomProp());
            property.load(fis2);
            for(Map.Entry<Object, Object> entry : property.entrySet()){
                CONFIGMAP.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()).trim());
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static String get(String key){
        if(Objects.isNull(key) || Objects.isNull(CONFIGMAP.get(key.toLowerCase()))) {
            try{
                throw new Exception("Property name '" + key + "' not found. Please check config.properties file under resources folder to see if it has been added.");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return CONFIGMAP.get(key.toLowerCase());
    }

    public static void set(String key, String value){
       CONFIGMAP.put(key, value);
    }
}
