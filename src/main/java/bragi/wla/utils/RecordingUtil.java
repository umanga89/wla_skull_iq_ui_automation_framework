package bragi.wla.utils;

import bragi.wla.driver.AppDriverManager;
import bragi.wla.setpath.VideoPath;
import io.appium.java_client.android.AndroidStartScreenRecordingOptions;
import io.appium.java_client.ios.IOSStartScreenRecordingOptions;
import io.appium.java_client.screenrecording.CanRecordScreen;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Base64;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RecordingUtil {

    public static void startRecording_android(){
        ((CanRecordScreen) AppDriverManager.getDriver()).startRecordingScreen(new AndroidStartScreenRecordingOptions()
                .withVideoSize(PropertyUtils.get("video.resolution"))
                .withTimeLimit(Duration.ofSeconds(1200)));
    }

    public static void startRecording_iOS(){
        ((CanRecordScreen) AppDriverManager.getDriver()).startRecordingScreen(new IOSStartScreenRecordingOptions()
                .withTimeLimit(Duration.ofSeconds(1200))
                .withVideoType("h264"));
    }

    public static void stopRecording(String testName){
        String base64String = ((CanRecordScreen)AppDriverManager.getDriver()).stopRecordingScreen();

        byte[] data = Base64.getDecoder().decode(base64String);
        String destinationPath= VideoPath.getVideoPath();
        Path path = Paths.get(destinationPath);
        try {
            Files.write(path, data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
