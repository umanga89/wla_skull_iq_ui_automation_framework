package bragi.wla.Appium;

import bragi.wla.utils.PropertyUtils;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Log4j2
public final class AppiumServer {

    public static HashMap<String, Object> startAppiumServer(){
        AppiumServiceBuilder builder = new AppiumServiceBuilder();
        //add carthage path if this is not working for iOS
        HashMap<String, Object> capabilities = null;
        try{
           if(PropertyUtils.get("run.mode").equalsIgnoreCase("local")){
               builder.usingPort(4723);
               builder.withAppiumJS(new File("/opt/homebrew/lib/node_modules/appium"));
               builder.usingDriverExecutable(new File("/opt/homebrew/bin/node"));
               builder.withLogFile(new File("test-log/appium-log.txt"));
               AppiumDriverLocalService service = AppiumDriverLocalService.buildService(builder);
               service.start();
               log.info("Appium server started");
               ServiceManager.setService(service);
           }
        }catch (Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
            System.exit(1);
        }
        return capabilities;
    }

    public static void stopServer(){
        Runtime runtime = Runtime.getRuntime();
        try{
            String[] command = {"/usr/bin/killall", "-9", "node"};
            runtime.exec(command);
            log.info("Appium server stopped");
            ServiceManager.unloadService();
            log.info("Appium server has been unloaded");
        } catch (IOException e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
}
