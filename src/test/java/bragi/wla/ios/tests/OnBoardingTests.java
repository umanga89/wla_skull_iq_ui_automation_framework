package bragi.wla.ios.tests;

import bragi.wla.Appium.AppiumServer;
import bragi.wla.BaseSetup;
import bragi.wla.driver.AppDriver;
import bragi.wla.pages.ios.OnBoardingPagesiOS;
import bragi.wla.reporter.ExtentLogger;
import bragi.wla.reporter.ExtentReport;
import bragi.wla.utils.FileSystemHandlerUtil;
import bragi.wla.utils.RecordingUtil;
import bragi.wla.utils.UploadReportToGoogleDriveUtil;
import lombok.extern.log4j.Log4j2;
import org.testng.Assert;
import org.testng.annotations.Test;

@Log4j2
public final class OnBoardingTests extends BaseSetup {

//    Only enable if tests in this class to group into a sub section
//    @BeforeClass
//    public void createTestSuite(){
//        ExtentReport.createTestSuite(this.getClass().getSimpleName(), "Test");
//    }

    @Test(testName = "Navigate user to home page after a successful completion of on-boarding",
            description = "Verify user is able to navigate to home screen after completing on-boarding steps",
            groups = {"functional"})
    public void onboardingFlow() throws InterruptedException {
        try {
            OnBoardingPagesiOS onBoardingPagesiOS = new OnBoardingPagesiOS();

            //Welcome screen actions
            onBoardingPagesiOS.waitUntilUserIsViewingWelcomeScreen();
            Assert.assertTrue(onBoardingPagesiOS.isSkullCandyLogoIsDisplayedInWelcomeScreen(), "Skullcandy logo is not displayed");
            ExtentLogger.pass("Welcome Screen is loaded");
            onBoardingPagesiOS.clickOnContinueButton();
            ExtentLogger.pass("T&C Screen is loaded");

            //Terms and conditions page
            onBoardingPagesiOS.agreeToTandCWithSlider();
            ExtentLogger.pass("Agreed to terms and conditions");

            //Enable bluetooth page
            Assert.assertTrue(onBoardingPagesiOS.isEnableBluetoothPageTitleDisplayed(), "User is not in enable bluetooth screen");
            ExtentLogger.pass("Enable bluetooth page is loaded");
            onBoardingPagesiOS.clickOnOKSoundsGoodButton();
            onBoardingPagesiOS.clickAllowBluetoothPermissionButton();

            //Enable push notifications page
            Assert.assertTrue(onBoardingPagesiOS.isEnablePushNotificationsPageTitleDisplayed(), "User is not in enable push notifications screen");
            ExtentLogger.pass("Enable push notifications page is loaded");
            onBoardingPagesiOS.clickOnPushNotificationsContinueButton();
            onBoardingPagesiOS.clickAllowPushNotificationsPermissionButton();

            //Enable push notifications page
            Assert.assertTrue(onBoardingPagesiOS.isLetsCheckYourSetupPageTitleDisplayed(), "User is not in let's check your setup screen");
            ExtentLogger.pass("Let's check your setup page is loaded");
            onBoardingPagesiOS.clickOnNowImConnectedButton();

            //Select your headphones page
            Assert.assertTrue(onBoardingPagesiOS.isConnectHeadphonesPageTitleDisplayed(), "User is not in connect headphones page");
            ExtentLogger.pass("Connect Headphones is loaded");

        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            ExtentLogger.fail(e.getMessage());
            RecordingUtil.stopRecording("onboardingFlow");
            ExtentLogger.addVideoToReport();
            ExtentReport.flushReport();
            AppDriver.closeApp();
            AppiumServer.stopServer();
            FileSystemHandlerUtil.createZipFileForReport();
            UploadReportToGoogleDriveUtil.uploadReport();
            System.exit(1);
        }
    }
}
