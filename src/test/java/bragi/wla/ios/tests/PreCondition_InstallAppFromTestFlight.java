package bragi.wla.ios.tests;

import bragi.wla.Appium.AppiumServer;
import bragi.wla.BaseSetup;
import bragi.wla.driver.AppDriver;
import bragi.wla.pages.ios.TestFlightAppiOS;
import bragi.wla.reporter.ExtentLogger;
import bragi.wla.reporter.ExtentReport;
import bragi.wla.utils.PropertyUtils;
import bragi.wla.utils.RecordingUtil;
import bragi.wla.utils.AppOperationsUtil;
import lombok.extern.log4j.Log4j2;
import org.testng.Assert;
import org.testng.annotations.Test;

@Log4j2
public final class PreCondition_InstallAppFromTestFlight extends BaseSetup {

//    Only enable if tests in this class to group into a sub section
//    @BeforeClass
//    public void createTestSuite(){
//        ExtentReport.createTestSuite(this.getClass().getSimpleName(), "Test");
//    }

    @Test(testName = "Install latest version of the app from TestFlight",
            description = "Latest Version of the app needs to be installed before starting tests",
            groups = {"functional"})
    public void installAppFromTestFlight() throws InterruptedException {
        try {

            //Uninstall existing app from connected device
            AppOperationsUtil.uninstallSkullCandyAppiOS();
            Thread.sleep(3000);

            TestFlightAppiOS testFlightAppiOS = new TestFlightAppiOS();

            //Wait until testflight app is loaded
            testFlightAppiOS.waitUntilUserIsViewingTestFlightApp();

            //Click install app
            testFlightAppiOS.clickInstallDevApp();

            //Wait until app is installed
            testFlightAppiOS.waitUntilDevAppInstallationIsComplete();
            Assert.assertTrue(testFlightAppiOS.isAppInstalled(), "App is not installed");
            ExtentLogger.pass("App installed successfully!");
            log.info("App installed successfully!");

            PropertyUtils.set("app.installed", "yes");

        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            ExtentLogger.fail(e.getMessage());
            RecordingUtil.stopRecording("appInstallTestFlight");
            ExtentLogger.addVideoToReport();
            ExtentReport.flushReport();
            AppDriver.closeApp();
            AppiumServer.stopServer();
            System.exit(1);
        }
    }
}
