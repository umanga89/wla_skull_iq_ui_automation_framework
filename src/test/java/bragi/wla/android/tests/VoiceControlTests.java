package bragi.wla.android.tests;

import bragi.wla.BaseSetup;
import bragi.wla.pages.android.EarBudsConnectedPageAndroid;
import bragi.wla.pages.android.OnBoardingPagesAndroid;
import bragi.wla.pages.android.VoiceControlPageAndroid;
import bragi.wla.reporter.ExtentLogger;
import lombok.extern.log4j.Log4j2;
import org.testng.Assert;
import org.testng.annotations.Test;

@Log4j2
public final class VoiceControlTests extends BaseSetup {

    @Test(testName = "Verify Voice Control is enabled by default", description = "Verify Voice Control is enabled by default", groups = {"functional"})
    public void vc_test_1() throws InterruptedException {
        try {
            //Connect earbuds and navigate to home page
            //This is no longer required
            //preCondition_Connect_EarBuds();

            EarBudsConnectedPageAndroid earBudsConnectedPageAndroid = new EarBudsConnectedPageAndroid();
            Thread.sleep(5000);
            //Click on Voice Control to open voice control panel
            earBudsConnectedPageAndroid.clickVoiceControlButton();
            log.info("Clicked on voice control button");

            //Wait until Voice Control panel is loaded
            VoiceControlPageAndroid voiceControlPageAndroid = new VoiceControlPageAndroid();
            voiceControlPageAndroid.waitUntilUserIsViewingVoiceControlPanel();

            //Verify if voice control header is displayed on screen
            Assert.assertTrue(voiceControlPageAndroid.isUserViewingVoiceControlPanel(), "User is not viewing Voice Control panel");
            log.info("Voice Control panel is loaded");
            ExtentLogger.pass("Voice Control panel is loaded");

            //Verify Enable Voice Control toggle is enabled by default
            Assert.assertTrue(voiceControlPageAndroid.isVoiceControlToggleEnabled(), "Voice Control toggle is not enabled by default");
            ExtentLogger.pass("Enable Voice Control toggle is enabled by default");
            log.info("Enable Voice Control toggle is enabled by default");

            //Verify English language is selected by default for voice control
            voiceControlPageAndroid.clickVoiceLanguageSelector();
            Assert.assertTrue(voiceControlPageAndroid.isEnglishLanguageSelectedForVoiceControl(), "English Language is not selected by default");
            ExtentLogger.pass("English Language is selected by default for voice control");
            log.info("English Language is selected by default for voice control");
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            ExtentLogger.fail(e.getMessage());
            throw e;
        }
    }

    @Test(testName = "Verify Voice Control can be toggled OFF", description = "Verify Voice Control can be toggled OFF and it is saved successfully", groups = {"functional"})
    public void vc_test_2() throws InterruptedException {
        try {

            //Connect earbuds and navigate to home page
            //This is no longer required
            //preCondition_Connect_EarBuds();

            EarBudsConnectedPageAndroid earBudsConnectedPageAndroid = new EarBudsConnectedPageAndroid();
            Thread.sleep(5000);
            //Click on Voice Control to open voice control panel
            earBudsConnectedPageAndroid.clickVoiceControlButton();
            log.info("Clicked on voice control button");

            //Wait until Voice Control panel is loaded
            VoiceControlPageAndroid voiceControlPageAndroid = new VoiceControlPageAndroid();
            voiceControlPageAndroid.waitUntilUserIsViewingVoiceControlPanel();

            //Verify if voice control header is displayed on screen
            Assert.assertTrue(voiceControlPageAndroid.isUserViewingVoiceControlPanel(), "User is not viewing Voice Control panel");
            log.info("Voice Control panel is loaded");
            ExtentLogger.pass("Voice Control panel is loaded");

            //Toggle OFF voice control
            voiceControlPageAndroid.clickVoiceControlToggle();
            Assert.assertFalse(voiceControlPageAndroid.isVoiceControlToggleEnabled(), "Voice Control toggle is still enabled");
            ExtentLogger.pass("Enable Voice Control toggle is OFF");
            log.info("Enable Voice Control toggle is OFF");

            //Close and open voice control panel to check if changes are saved
            voiceControlPageAndroid.clickCloseVoiceControlPanel();
            earBudsConnectedPageAndroid.waitUntilUserIsViewingEarbudsPage();
            Assert.assertTrue(earBudsConnectedPageAndroid.isEarbudsInfoIsDisplayedInEarbudsScreen(), "User is not viewing Ear Buds Info");
            log.info("Voice Control panel is closed");
            ExtentLogger.pass("Voice Control panel is closed");
            earBudsConnectedPageAndroid.clickVoiceControlButton();

            voiceControlPageAndroid.waitUntilUserIsViewingVoiceControlPanel();
            Assert.assertTrue(voiceControlPageAndroid.isUserViewingVoiceControlPanel(), "User is not viewing Voice Control panel");
            log.info("Voice Control panel is loaded");
            ExtentLogger.pass("Voice Control panel is loaded");

            Assert.assertFalse(voiceControlPageAndroid.isVoiceControlToggleEnabled(), "Voice Control toggle is still enabled");
            ExtentLogger.pass("Enable Voice Control toggle is OFF");
            log.info("Enable Voice Control toggle is OFF");
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            ExtentLogger.fail(e.getMessage());
            throw e;
        }
    }

    @Test(testName = "Verify Voice Control can be toggled ON", description = "Verify Voice Control can be toggled ON and it is saved successfully", groups = {"functional"})
    public void vc_test_3() throws InterruptedException {
        try {

            //Connect earbuds and navigate to home page
            //This is no longer required
            //preCondition_Connect_EarBuds();

            EarBudsConnectedPageAndroid earBudsConnectedPageAndroid = new EarBudsConnectedPageAndroid();
            Thread.sleep(5000);
            //Click on Voice Control to open voice control panel
            earBudsConnectedPageAndroid.clickVoiceControlButton();
            log.info("Clicked on voice control button");

            //Wait until Voice Control panel is loaded
            VoiceControlPageAndroid voiceControlPageAndroid = new VoiceControlPageAndroid();
            voiceControlPageAndroid.waitUntilUserIsViewingVoiceControlPanel();

            //Verify if voice control header is displayed on screen
            Assert.assertTrue(voiceControlPageAndroid.isUserViewingVoiceControlPanel(), "User is not viewing Voice Control panel");
            log.info("Voice Control panel is loaded");
            ExtentLogger.pass("Voice Control panel is loaded");

            //Toggle OFF voice control
            voiceControlPageAndroid.clickVoiceControlToggle();
            Assert.assertTrue(voiceControlPageAndroid.isVoiceControlToggleEnabled(), "Voice Control toggle is still disabled");
            ExtentLogger.pass("Enable Voice Control toggle is ON");
            log.info("Enable Voice Control toggle is ON");

            //Close and open voice control panel to check if changes are saved
            voiceControlPageAndroid.clickCloseVoiceControlPanel();
            earBudsConnectedPageAndroid.waitUntilUserIsViewingEarbudsPage();
            Assert.assertTrue(earBudsConnectedPageAndroid.isEarbudsInfoIsDisplayedInEarbudsScreen(), "User is not viewing Ear Buds Info");
            log.info("Voice Control panel is closed");
            ExtentLogger.pass("Voice Control panel is closed");
            earBudsConnectedPageAndroid.clickVoiceControlButton();

            voiceControlPageAndroid.waitUntilUserIsViewingVoiceControlPanel();
            Assert.assertTrue(voiceControlPageAndroid.isUserViewingVoiceControlPanel(), "User is not viewing Voice Control panel");
            log.info("Voice Control panel is loaded");
            ExtentLogger.pass("Voice Control panel is loaded");

            Assert.assertTrue(voiceControlPageAndroid.isVoiceControlToggleEnabled(), "Voice Control toggle is still disabled");
            ExtentLogger.pass("Enable Voice Control toggle is ON");
            log.info("Enable Voice Control toggle is ON");
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            ExtentLogger.fail(e.getMessage());
            throw e;
        }
    }

    public static void preCondition_Connect_EarBuds(){
        try{
            OnBoardingPagesAndroid onBoardingPagesAndroid = new OnBoardingPagesAndroid();

            //Select your headphones page
            Assert.assertTrue(onBoardingPagesAndroid.isConnectHeadphonesPageTitleDisplayed(), "User is not in connect headphones page");
            ExtentLogger.pass("Connect Headphones is loaded");
            log.info("Connect Headphones is loaded");
            Assert.assertTrue(onBoardingPagesAndroid.isAvailableDevicesDisplayed(), "Available devices not displayed");
            onBoardingPagesAndroid.clickOnConnectSkullCandyGrindButton();
            onBoardingPagesAndroid.clickOnPairButton();

            //Earbuds screen
            EarBudsConnectedPageAndroid earBudsConnectedPageAndroid = new EarBudsConnectedPageAndroid();
            earBudsConnectedPageAndroid.waitUntilUserIsViewingEarbudsPage();
            Assert.assertTrue(earBudsConnectedPageAndroid.isEarbudsInfoIsDisplayedInEarbudsScreen(), "User is not in earbuds screen");
            ExtentLogger.pass("Earbuds page is loaded");
            log.info("Earbuds page is loaded");
            Assert.assertEquals(earBudsConnectedPageAndroid.getEarbudsTitle(), "Grind", "Earbuds title is not Grind");
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            ExtentLogger.fail(e.getMessage());
            throw e;
        }
    }
}
