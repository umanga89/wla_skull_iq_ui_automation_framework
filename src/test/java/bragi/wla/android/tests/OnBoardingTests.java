package bragi.wla.android.tests;

import bragi.wla.Appium.AppiumServer;
import bragi.wla.BaseSetup;
import bragi.wla.constants.GlobalVars;
import bragi.wla.driver.AppDriver;
import bragi.wla.driver.AppDriverManager;
import bragi.wla.pages.android.AppTesterPagesAndroid;
import bragi.wla.pages.android.EarBudsConnectedPageAndroid;
import bragi.wla.pages.android.OnBoardingPagesAndroid;
import bragi.wla.reporter.ExtentLogger;
import bragi.wla.reporter.ExtentReport;
import bragi.wla.utils.FileSystemHandlerUtil;
import bragi.wla.utils.PropertyUtils;
import bragi.wla.utils.RecordingUtil;
import bragi.wla.utils.UploadReportToGoogleDriveUtil;
import lombok.extern.log4j.Log4j2;
import org.testng.Assert;
import org.testng.annotations.Test;

@Log4j2
public final class OnBoardingTests extends BaseSetup {

//    Only enable if tests in this class to group into a sub section
//    @BeforeClass
//    public void createTestSuite(){
//        ExtentReport.createTestSuite(this.getClass().getSimpleName(), "Test");
//    }

    @Test(testName = "Navigate user to home page after a successful completion of on-boarding",
            description = "Verify user is able to navigate to home screen after completing on-boarding steps",
            groups = {"functional"})
    public void onboardingFlow() throws InterruptedException {
        try {

            //Welcome screen actions
            OnBoardingPagesAndroid onBoardingPagesAndroid = new OnBoardingPagesAndroid();

            onBoardingPagesAndroid.waitUntilUserIsViewingWelcomeScreen();
            Assert.assertTrue(onBoardingPagesAndroid.isSkullCandyLogoIsDisplayedInWelcomeScreen(), "Skullcandy logo is not displayed");
            ExtentLogger.pass("Welcome Screen is loaded");
            log.info("Welcome Screen is loaded");
            onBoardingPagesAndroid.clickOnContinueButton();
            ExtentLogger.pass("T&C Screen is loaded");
            log.info("T&C Screen is loaded");

            //Terms and conditions page
            onBoardingPagesAndroid.agreeToTandCWithSlider();
            ExtentLogger.pass("Agreed to terms and conditions");
            log.info("Agreed to terms and conditions");

            //Enable bluetooth page
            Assert.assertTrue(onBoardingPagesAndroid.isEnableBluetoothPageTitleDisplayed(), "User is not in enable bluetooth screen");
            ExtentLogger.pass("Enable bluetooth page is loaded");
            log.info("Enable bluetooth page is loaded");
            onBoardingPagesAndroid.clickOnOKSoundsGoodButton();
            onBoardingPagesAndroid.clickAllowBluetoothPermissionButton();

            //Enable file downloads page
            Assert.assertTrue(onBoardingPagesAndroid.isEnableFileDownloadsPageTitleDisplayed(), "User is not in enable file downloads page");
            ExtentLogger.pass("Enable file downloads page is loaded");
            log.info("Enable file downloads page is loaded");
            //onBoardingPagesAndroid.getAllowDownloadsText();
            log.info(AppDriverManager.getDriver().getPageSource());
            onBoardingPagesAndroid.clickOnContinueAllowDownloadsButton();
            onBoardingPagesAndroid.clickAllowFileDownloadsPermissionButton();

            //Enable location page
            Assert.assertTrue(onBoardingPagesAndroid.isEnableLocationPermissionPageTitleDisplayed(), "User is not in enable location page");
            ExtentLogger.pass("Enable location page is loaded");
            log.info("Enable location page is loaded");
            Thread.sleep(3000);
            //onBoardingPagesAndroid.getAllowLocationText();
            log.info(AppDriverManager.getDriver().getPageSource());
            onBoardingPagesAndroid.clickOnContinueAllowLocationButton();
            onBoardingPagesAndroid.clickAllowLocationPermissionButton();

            //Select your headphones page
            Assert.assertTrue(onBoardingPagesAndroid.isConnectHeadphonesPageTitleDisplayed(), "User is not in connect headphones page");
            ExtentLogger.pass("Connect Headphones is loaded");
            log.info("Connect Headphones is loaded");
            Assert.assertTrue(onBoardingPagesAndroid.isAvailableDevicesDisplayed(), "Available devices not displayed");
            onBoardingPagesAndroid.clickOnConnectSkullCandyGrindButton();
            //No longer displayed in UI
            //onBoardingPagesAndroid.clickOnPairButton();

            //Earbuds screen
            EarBudsConnectedPageAndroid earBudsConnectedPageAndroid = new EarBudsConnectedPageAndroid();
            earBudsConnectedPageAndroid.waitUntilUserIsViewingEarbudsPage();
            Assert.assertTrue(earBudsConnectedPageAndroid.isEarbudsInfoIsDisplayedInEarbudsScreen(), "User is not in earbuds screen");
            ExtentLogger.pass("Earbuds page is loaded");
            log.info("Earbuds page is loaded");
            Assert.assertEquals("Grind", earBudsConnectedPageAndroid.getEarbudsTitle(), "Earbuds title is not Grind");

            //Verify if both earbuds are connected and shows battery percentage
            Assert.assertEquals(earBudsConnectedPageAndroid.getLeftEarbudText(), "L", "Left earbud text is not shown in earbuds panel");
            Assert.assertEquals(earBudsConnectedPageAndroid.getRightEarbudText(), "R", "Right earbud text is not shown in earbuds panel");
            Assert.assertTrue(isValueNumeric(earBudsConnectedPageAndroid.getLeftEarbudBatteryText()), "Left earbud battery percentage is not correctly displayed");
            Assert.assertTrue(isValueNumeric(earBudsConnectedPageAndroid.getRightEarbudBatteryText()), "Right earbud battery percentage is not correctly displayed");
            Assert.assertTrue(earBudsConnectedPageAndroid.isLeftEarBudBatteryPercentageDisplayed(), "Left earbud percentage icon is not displayed");
            Assert.assertTrue(earBudsConnectedPageAndroid.isRightEarBudBatteryPercentageDisplayed(), "Right earbud percentage icon is not displayed");
            ExtentLogger.pass("Verified earbuds connection and battery details");
            log.info("Verified earbuds connection and battery details");
            //Setting initial run as false to avoid resetting app
            PropertyUtils.set("initial.run", "no");
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            ExtentLogger.fail(e.getMessage());
            PreCondition_InstallAppFromAppTester.processesUponFailingInitialTest();
        }
    }

    public static boolean isValueNumeric(String value){
        try {
            int d = Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
