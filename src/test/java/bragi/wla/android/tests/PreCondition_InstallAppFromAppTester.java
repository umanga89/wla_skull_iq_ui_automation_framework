package bragi.wla.android.tests;

import bragi.wla.Appium.AppiumServer;
import bragi.wla.BaseSetup;
import bragi.wla.driver.AppDriver;
import bragi.wla.pages.android.AppTesterPagesAndroid;
import bragi.wla.pages.android.EarBudsConnectedPageAndroid;
import bragi.wla.pages.android.OnBoardingPagesAndroid;
import bragi.wla.reporter.ExtentLogger;
import bragi.wla.reporter.ExtentReport;
import bragi.wla.utils.*;
import lombok.extern.log4j.Log4j2;
import org.testng.Assert;
import org.testng.annotations.Test;

@Log4j2
public final class PreCondition_InstallAppFromAppTester extends BaseSetup {

//    Only enable if tests in this class to group into a sub section
//    @BeforeClass
//    public void createTestSuite(){
//        ExtentReport.createTestSuite(this.getClass().getSimpleName(), "Test");
//    }

    @Test(testName = "Install App from AppTester",
            description = "Verify App can be installed from AppTester",
            groups = {"functional"})
    public void install_App_From_AppTester() throws InterruptedException {
        try {

            //Uninstall existing app from connected device
            AppOperationsUtil.uninstallSkullCandyAppAndroid();
            Thread.sleep(3000);

            //Install app from AppTester
            AppTesterPagesAndroid appTesterPagesAndroid = new AppTesterPagesAndroid();

            appTesterPagesAndroid.clickSignInWithGoogle();
            appTesterPagesAndroid.clickGoogleAccount();
            appTesterPagesAndroid.clickSkulliQDevVersion();
            appTesterPagesAndroid.clickConsentCheckBox();
            appTesterPagesAndroid.clickConsentButton();
            appTesterPagesAndroid.clickDownloadButtonOfLatestVersion();
            ExtentLogger.pass("Downloading latest version of the app");
            log.info("Downloading latest version of the app");
            appTesterPagesAndroid.clickInstallApp();
            ExtentLogger.pass("Installing latest version of the app");
            log.info("Installing latest version of the app");
            appTesterPagesAndroid.isAppInstalled();
            ExtentLogger.pass("App is successfully installed");
            log.info("App is successfully installed");

            PropertyUtils.set("app.installed", "yes");

        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            ExtentLogger.fail(e.getMessage());
            processesUponFailingInitialTest();
        }
    }


    public static void processesUponFailingInitialTest(){
        if(PropertyUtils.get("run.mode").equalsIgnoreCase("local")) {
            RecordingUtil.stopRecording("appInstallAppTester");
            ExtentLogger.addVideoToReport();
        }
        ExtentReport.flushReport();
        AppDriver.closeApp();
        AppiumServer.stopServer();
        FileSystemHandlerUtil.createZipFileForReport();
        UploadReportToGoogleDriveUtil.uploadReport();
        System.exit(1);
    }
}
