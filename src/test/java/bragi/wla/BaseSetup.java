package bragi.wla;

import bragi.wla.driver.AppDriver;
import bragi.wla.reporter.ExtentLogger;
import bragi.wla.utils.PropertyUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class BaseSetup {

    @BeforeMethod(alwaysRun = true)
    protected void setUp(ITestContext context) throws Exception {
        try{
            //String platform = context.getCurrentXmlTest().getParameter("platform");
            String platform = PropertyUtils.get("platform");
            log.info("Test is running on "+platform+" platform");
            switch (platform){
                case "android":
                    if(PropertyUtils.get("app.installed").equalsIgnoreCase("no")) {
                        AppDriver.android_LaunchAppTester();
                    }else {
                        if (PropertyUtils.get("initial.run").equalsIgnoreCase("yes")) {
                            AppDriver.android_LaunchApp();
                        } else {
                            AppDriver.android_LaunchApp_no_rest_true();
                        }
                    }
                break;
                case "ios":
                    if(PropertyUtils.get("app.installed").equalsIgnoreCase("no")) {
                        AppDriver.iOS_LaunchTestFlight();
                    }else {
                        AppDriver.iOS_LaunchApp();
                    }
                break;
                default: throw new Exception("platform parameter is not defined/not matched with expected values (android/ios). Please check commandline argument");
            }
            log.info("App has been launched successfully!");
            log.info("Test has been started!!!!!!!");
        } catch (Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
            ExtentLogger.fail(e.getMessage());
            throw  e;

        }
    }

    @AfterMethod(alwaysRun = true)
    protected void tearDown() {
        String platform = PropertyUtils.get("platform");
        log.info("Test is running on "+platform+" platform");
        switch (platform) {
            case "android":
                if (PropertyUtils.get("initial.run").equalsIgnoreCase("no")) {
                    AppDriver.terminateApp_android();
                    log.info("App has been terminated successfully!");
                }
                break;
            case "ios":
                log.info("iOS after test!");
                break;
        }
        AppDriver.closeApp();
        log.info("App has been closed successfully!");
        log.info("Test has been ended!!!!!!!");
    }
}
