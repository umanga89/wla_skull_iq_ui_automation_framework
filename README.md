# How to setup your local environment to run automated tests (MacOS)

To execute the automated tests on your local machine, you need to configure it.

## Install Java 12

Use the following link to download java 12 based on your operating system. https://www.oracle.com/java/technologies/javase/jdk12-archive-downloads.html

## Install android studio 

Use following link to install android studio https://developer.android.com/studio

## Install Maven

Download maven binary from https://maven.apache.org/download.cgi (Download the apache-maven-x.x.x-bin.zip file)

## Install homebrew

Use https://brew.sh/ to install homebrew in your machine

## Add following to .zshrc profile to configure in your local environment

1) Navigate to home directory of current user using terminal

`cd ~`

2) Create a new .zshrc file

`vi .zshrc`

3) Add following content to the file. (Add correct location based on your installation for MAVEN_HOME)

`export JAVA_HOME=$(/usr/libexec/java_home)`
`export MAVEN_HOME=/Users/<user>/Desktop/software/maven/apache-maven-3.8.6`
`export ANDROID_HOME=/Users/<user>/Library/Android/sdk`
`export BREW_HOME=/opt/homebrew`

`export PATH=${JAVA_HOME}/bin:$MAVEN_HOME/bin:$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$BREW_HOME/bin:$PATH`

4) Save file and run following command to apply changes

`source ~/.zshrc`

## Install Carthage via homebrew

Use terminal to install Carthage

`brew install carthage`

## Install node via homebrew

Use terminal to install Carthage

`brew install node`

Run `brew upgrade node` to upgrade node to latest version

## Install Xcode (iOS Specific)

Download and install Xcode from AppStore

## Install Xcode commandline tools (iOS Specific)

## Install ios-deploy (iOS Specific)

Use terminal to install ios-deploy which is used to install and debug iOS apps through the command line without using the Xcode

`brew install ios-deploy`

## Install ideviceinstaller to manipulate iOS applications (install, update, uninstall etc) (iOS Specific)

Use terminal to install ideviceinstaller

`brew install ideviceinstaller`

## Install appium via node

Use terminal to install appium 2.0

`npm install -g appium@next`

## Install xcuitest and uiautomator2 drivers

`appium driver install xcuitest`

`appium driver install uiautomator2`

## (OPTIONAL) Install Appium desktop client and Appium Inspector (If you need to start Appium Server via GUI client. Appium inspector is used to inspect mobile elements)

Appium Desktop => https://github.com/appium/appium-desktop
Appium Inspector => https://github.com/appium/appium-inspector

## Configure WebDriverAgent v4.9.0 (Working version at the moment) to run tests on iOS real device (iOS Specific)

WebDriverAgent which is coming with appium installation is having an issue supporting test exection on iOS real devices. Please update it to the given version. 

1) Find the WebDriverAgent installation in your machine

/Users/{user_name}/.npm-global/lib/node_modules/appium/node_modules/appium-webdriveragent

2) Remove the files in the above directory and replace it with the files in following link
https://github.com/appium/WebDriverAgent/releases/tag/v4.9.0

3) Open the WebDriverAgent.xcodeproj project in Xcode

Note - You need to sign in with your apple id in xcode (also have your apple id added to the bragi development team)

Once the project is opened in xcode, click on the webdriveragent on left side panel

The main project name is WebDriverAgent. Inside this project, there are two Targets
a. WebDriverAgentLib
b. WebDriverAgentRunner

- Click on WebDriverAgentLib

- Click on Build settings

- Scroll to Packaging and in Product Bundle Identifier, there’s written some string like com.facebook.WebDriverAgentLib. Change “facebook” to some unique string like “abc123” so that it becomes like this com.abc123.WebDriverAgentLib

- Click Signing & Capabilities and select Automatically manage signing

- Select the account that you created at step 8 in the Team dropdown
Note - Bundle Identifier should be the one you have given in step 12

- Select Development in Signing Certificate. It’ll generate a signing certificate for you

- Now select WebDriverAgentRunner and repeat the steps from 11 to 16.

## Install ffmpeg via homebrew

This is needed to support longer video recordings for iOS tests

`brew install ffmpeg`

## Get the device udid for Android and iOS real devices connect to your machine

Android => `adb devices`
Sample response
---------------
`List of devices attached`
`RF8T915GRZR	device`

iOS => `ios-deploy -c`
Sample response
---------------
`[....] Waiting up to 5 seconds for iOS device to be connected`
`[....] Found 00008030-001939C20207802E (D79AP, iPhone SE 2G, iphoneos, arm64e, 16.1, 20B82) a.k.a. 'BGiOS01' connected through USB.`

## Install WebDriverAgent in your real device (iOS Specific)

Navigate to /Users/{user_name}/.npm-global/lib/node_modules/appium/node_modules/appium-webdriveragent and run the following command in terminal to intall webdriveragent in real device

`xcodebuild -project WebDriverAgent.xcodeproj -scheme WebDriverAgentRunner -destination id=<deviceid> test -allowProvisioningUpdates`

# How to execute tests on real devices

1) Open the testsuite using an IDE (IntelliJ IDEA is recommended)

Clone the git repository and open the testsuite in IDE

2) Open config.properties file located in src/test/resources/config/config.properties

- Update `ios.app.location` and `android.app.location` with correct locations for your ipa and apk files of the app. If you already have installed app in your device and you need to use that instance, ignore this step.

- Update `use.existing.app` property to yes if you want to use already installed apps in your devices

Save changes accordingly.

3) Navigate to directory where pom.xml file is available using terminal

To run android tests => Run `mvn clean install` command

To run iOS tests => Run `mvn clean install -Dplatform=ios -DtestngXml=testng_ios.xml`

4) Review test artifacts generated after an execution

HTML Report with screenshots and videos => test-reports/ExecutionReport_xxxxx.html
Videos => test-reports/test-report-videos/
Logs => test-log/test-log-xxxx.txt



